# Itbid UI Kit

## Introducción

En este documento vamos a intentar explicar de la forma más clara posible la estructura que  tiene el proyecto
y la forma de instalarlo en un proyecto externo.

## Estructura de ficheros

La estructura de este proyecto debe cumplir ciertos requisitos.

1. El código fuente del proyecto debe ir dentro de la carpeta src.
2. Los componentes deben ir dentro de la carpeta components y cada uno debe tener su propia carpeta
3. Todos los componentes deberán tener el mismo prefijo ib (ej. ibInput, ibAlert, ibDatepicker).
4. Toda la documentación deberá ir dentro de la carpeta docs y su formato será  [Markdown](https://www.markdownguide.org/cheat-sheet/)
5. Todos los componentes serán vue nativos y **no se podrán utilizar librerías de terceros**.
