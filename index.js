import * as components from './components'

const itbidUiKit = {
    name: 'uiKit',
    install(app) {
        for (const key in components) {
            const component = components[key]
            if (component) {
                app.component(key, component)
            }
        }
    },
}

export default itbidUiKit;