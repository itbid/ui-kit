import ibSubmenu from './ibSubmenu.vue'
import ibSubmenuItem from './ibSubmenuItem.vue'

export { ibSubmenu, ibSubmenuItem }

export default ibSubmenu