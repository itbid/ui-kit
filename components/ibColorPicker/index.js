import ibColorPicker from './ibColorPicker.vue'
import ibPicker from './ibPicker.vue'

export { ibColorPicker, ibPicker }

export default ibColorPicker