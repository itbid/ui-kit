import ibBreadcrumb from './ibBreadcrumb.vue'
import ibBreadcrumbItem from './ibBreadcrumbItem.vue'

export { ibBreadcrumb, ibBreadcrumbItem }

export default ibBreadcrumb