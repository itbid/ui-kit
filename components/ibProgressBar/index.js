import ibProgressBar from './ibProgressBar.vue'
import ibProgressBarPassword from './ibProgressBarPassword.vue'
import ibProgress from './ibProgress.vue'
export { ibProgressBar, ibProgressBarPassword , ibProgress }

export default ibProgressBar
