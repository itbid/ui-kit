import ibSidebar from './ibSidebar.vue'
import ibMenuLink from './ibMenuLink.vue'

export { ibSidebar, ibMenuLink }

export default ibSidebar