import ibDataTable from './ibDataTable.vue'
import ibDataTableFilter from './ibDataTableFilter.vue'

export { ibDataTable, ibDataTableFilter }

export default ibDataTable