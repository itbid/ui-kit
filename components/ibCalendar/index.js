import ibCalendar from './ibCalendar.vue'
import ibCalendarBase from './ibCalendarBase.vue'
import ibCalendarDoble from './ibCalendarDoble.vue'

export { ibCalendar, ibCalendarBase, ibCalendarDoble }

export default ibCalendar